import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import '../styles/Footer.css';

const Footer = () => {
  return (
    <footer className="footer-container">
      <div className="footer-content">
        <div className="footer-info">
          <div className="footer-info-item">
            <i className="fas fa-phone footer-icon"></i>
            <p>+39 2436596504</p>
          </div>
          <div className="footer-info-item">
            <i className="fas fa-map-marker-alt footer-icon"></i>
            <p>34 Grove Street, Los Angeles</p>
            <p>Country</p>
          </div>
          <div className="footer-info-item">
            <i className="fas fa-envelope footer-icon"></i>
            <p>contact@littlelemon.com</p>
          </div>
        </div>
      </div>
      <div className="footer-copyright">
        <p>&copy; 2023 Little Lemon. All rights reserved.</p>
      </div>
    </footer>
  );
}

export default Footer;



