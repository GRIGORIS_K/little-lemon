import React from 'react';
import '../styles/Nav.css';

const Nav = () => {
  return (
    <nav className="nav-container">
      <ul className="nav-list">
        <li className="nav-item"><a href="/home">Home</a></li>
        <li className="nav-item"><a href="/about">About</a></li>
        <li className="nav-item"><a href="/services">Services</a></li>
        <li className="nav-item"><a href="/reservations">Reservations</a></li>
        <li className="nav-item"><a href="/reservations">Orders</a></li>
        <li className="nav-item"><a href="/contact">Contact</a></li>
      </ul>
    </nav>
  );
}

export default Nav;
