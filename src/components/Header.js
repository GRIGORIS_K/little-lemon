import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import '../styles/Header.css';
import logo from '../images/logo.png';

const Header = () => {
  return (
    <header>
      <div className="header-container">
        <img src={logo} alt="Little Lemon Logo" />
        <div className="header-social-media">
          <a href="https://www.facebook.com/your-page" target="_blank" rel="noopener noreferrer">
            <i className="fab fa-facebook"></i>
          </a>
          <a href="https://www.instagram.com/your-profile" target="_blank" rel="noopener noreferrer">
            <i className="fab fa-instagram"></i>
          </a>
          <a href="https://www.twitter.com/your-handle" target="_blank" rel="noopener noreferrer">
            <i className="fab fa-twitter"></i>
          </a>
          <a href="https://www.youtube.com/your-channel" target="_blank" rel="noopener noreferrer">
            <i className="fab fa-youtube"></i>
          </a>
      </div>
      </div>
    </header>
  );
}

export default Header;
