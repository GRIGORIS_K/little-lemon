import React from 'react';
import Banner from './Banner';
import FeaturedContent from './FeaturedContent';
import CallToAction from './CallToAction';

const HomePage = () => {
  return (
    <div>
      <Banner />
      <FeaturedContent />
      <CallToAction />
    </div>
  );
}

export default HomePage;
