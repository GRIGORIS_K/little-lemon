import React from 'react';
import { Helmet } from 'react-helmet';
import Header from './components/Header';
import Nav from './components/Nav';
import Main from './components/Main';
import Footer from './components/Footer';
import logo from './images/logo.png';

const App = () => {
  return (
    <>
      <Helmet>
        <meta property="og:title" content="Little lemon restaurant" />
        <meta property="og:description" content="Come and taste delicious flavors. Open Tuesday to Sunday, 18:00 to 02:00, in the center of Athens" />
        <meta property="og:image" content={logo} />
      </Helmet>
      <Header />
      <Nav />
      <Main />
      <Footer />
    </>
  );
}

export default App;
